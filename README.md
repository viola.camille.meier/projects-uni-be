The following projects were created during the Programming 2 course within a team of three individuals. 
For each project, the 'Contributions.md' file documents the specific classes authored by each team member.
I changed the names from the other team members, so that they remain anonymous.
