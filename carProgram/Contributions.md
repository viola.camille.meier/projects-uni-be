One of the important aspects of software engineering is collaboration. The aim of this course is also to facilitate the environment to collaborate with each other. Therefore, the exercises are designed for explicit collaboration.
We expect each of you to take certain responsibilities for the exercise and describe them here. However, it is up to you who wants to do what part of the exercise.
To adhere to the P2 rules (each member should have at least 20% commits spread throughout the semester), please mention your contributions here.

- Replace `Person<Number>` with your name and describe the tasks you have contributed to this exercise. 

Viola Meier: Implemented CommandParser class

Tim Ambühl: Implemented Car Class and added test for Car class

Kevin Lautenschlager:
Made an early draft.
Implemented Trace class
Made the boardMaker, UML sequence diagram together with Viola

