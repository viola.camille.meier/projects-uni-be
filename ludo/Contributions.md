One of the important aspects of software engineering is collaboration. The aim of this course is also to facilitate the environment to collaborate with each other. Therefore, the exercises are designed for explicit collaboration.
We expect each of you to take certain responsibilities for the exercise and describe them here. However, it is up to you who wants to do what part of the exercise.
To adhere to the P2 rules (each member should have at least 20% commits spread throughout the semester), please mention your contributions here.

- Replace `Person<Number>` with your name and describe the tasks you have contributed to this exercise. 

team member 1: created Piece, Player and Movement class. Created Tests for all of them. Created first version of Game

Viola Meier: Implemented all classes in the Square directory,
             initialised the Squares in Game class,
             wrote tests for the Game class and all Square classes.

team member 2: Created render Module and Tests. UML diagram. Driver class. Die test. 
